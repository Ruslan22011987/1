﻿using System;
using Xunit;
using System.Collections.Generic;
using Tests2.Api.Reverence.Get;
using System.Reflection;

namespace Tests2
{
    public class HotelsCheckForEmptiness
    {
        DateTime dateTime = DateTime.Now;
        Program program = new Program();
        [Fact]
        public void Fact7HotelsSearch()
        {
            //для сохранения в базу данных.
            //копи паст для остальных.

            DateTime checkin = program.Friday_Sunday(dateTime);
            DateTime checkOut = checkin.AddDays(2);
            var id_post_hotel = program.Post_Hotel(checkin, checkOut, 513, 2);

            //метод для типа таймспан. время выполнения теста
            AddDBEntry bd = new AddDBEntry();
            bd.СreateAnEntryInTheDatabase(dateTime, id_post_hotel != 0, MethodInfo.GetCurrentMethod().Name);

            Assert.True(id_post_hotel != 0);
        }
        [Fact]
        public void Fact7HotelsStatus()
        {

            DateTime now = DateTime.Now;
            DateTime checkin = program.Friday_Sunday(dateTime);
            DateTime checkOut = checkin.AddDays(2);
            var id_post_hotel = program.Post_Hotel(checkin, checkOut, 513, 2);
            Assert.True(id_post_hotel != 0);

            var status_otel = program.Get_status(id_post_hotel, "/hotel/status?RequestId=");

            AddDBEntry bd = new AddDBEntry();
            bd.СreateAnEntryInTheDatabase(now, status_otel, MethodInfo.GetCurrentMethod().Name);

            Assert.True(status_otel);

        }
        [Fact]
        public void Fact7HotelsResulsts()
        {

            DateTime now = DateTime.Now;
            DateTime checkin = program.Friday_Sunday(dateTime);
            DateTime checkOut = checkin.AddDays(2);
            var id_post_hotel = program.Post_Hotel(checkin, checkOut, 513, 2);
            Assert.True(id_post_hotel != 0);

            var status_otel = program.Get_status(id_post_hotel, "/hotel/status?RequestId=");
            if (status_otel)
            {
                Hotel_json_results id_is_details = program.Get_hotel_results(id_post_hotel, dateTime.ToString());

                AddDBEntry bd = new AddDBEntry();
                bd.СreateAnEntryInTheDatabase(now, id_is_details.items.Count != null && id_is_details.items.Count != 0, MethodInfo.GetCurrentMethod().Name);
                //добавлено еще булево 
                Assert.True(id_is_details.items.Count != null && id_is_details.items.Count != 0);
            }
        }
        [Fact]
        public void Fact7HotelsDetails()
        {

            DateTime now = DateTime.Now;
            DateTime checkin = program.Friday_Sunday(dateTime);
            DateTime checkOut = checkin.AddDays(2);
            var id_post_hotel = program.Post_Hotel(checkin, checkOut, 513, 2);
            Assert.True(id_post_hotel != 0);

            var status_otel = program.Get_status(id_post_hotel, "/hotel/status?RequestId=");
            if (status_otel)
            {
                Hotel_json_results id_is_details = program.Get_hotel_results(id_post_hotel, dateTime.ToString());
                Assert.True(id_is_details.items != null && id_is_details.items.Count != 0);
                var bookgash = program.Get_hotel_details(id_post_hotel, id_is_details.items[0].hotelId);
                AddDBEntry bd = new AddDBEntry();
                bd.СreateAnEntryInTheDatabase(now, bookgash != null, MethodInfo.GetCurrentMethod().Name);
                Assert.True(bookgash != null);
            }
        }
        //не стоит условия проверки!!! нужно понять что проверять на пустоту
        [Fact]
        public void Fact7HotelsBuyDetails()
        {

            DateTime now = DateTime.Now;
            //DateTime dateTime7 = new DateTime(2018, 09, 15);
            DateTime checkin = program.Friday_Sunday(dateTime);
            DateTime checkOut = checkin.AddDays(2);
            var id_post_hotel = program.Post_Hotel(checkin, checkOut, 513, 2);
            Assert.True(id_post_hotel != 0);

            var status_otel = program.Get_status(id_post_hotel, "/hotel/status?RequestId=");
            if (status_otel)
            {
                Hotel_json_results id_is_details = program.Get_hotel_results(id_post_hotel, dateTime.ToString());
                Assert.True(id_is_details.items.Count != 0);
                var bookgash = program.Get_hotel_details(id_post_hotel, id_is_details.items[0].hotelId);
                Assert.True(bookgash != null);
                string stroka = bookgash.hotelDetails.rooms[0].bookHash;
                var otvetBuy = program.Get_hotel_buydetails(stroka, id_post_hotel, id_is_details.items[0].hotelId);
                //otvetBuy.hotelDetails.
                AddDBEntry bd = new AddDBEntry();
                bd.СreateAnEntryInTheDatabase(now, otvetBuy.hotelDetails.rooms[0].bookHash != null, MethodInfo.GetCurrentMethod().Name);

                Assert.True(otvetBuy.hotelDetails.rooms[0].bookHash != null);
            }
        }

    }
}
