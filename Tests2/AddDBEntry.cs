﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.Data.SQLite;
using System.Reflection;

namespace Tests2
{
    //объект который будет заполнятся в тестах, и отправляется на хранения в базу данных
    public class AddDBEntry
    {
        //уникальный номер генерируется автоматически
        
        public int id { get; set; }
        //имя выполняемоего метода string currMethodName =  System.Reflection.MethodInfo.GetCurrentMethod().Name;							
        public string nameMetod { get; set; }
        //берем из ассерт
        public string result { get; set; }
        //начало метода ? пишим в ручную или используем диагностик?*
        // DateTime.Now();
        public string startMetod { get; set; }
        public string stopMetod { get; set; }
        //дата запуска метода. Может генеририться автоматически?

        public string leadTime { get; set; }


        /// <summary>
        /// метод принимает старт тайм время начало работы метода, проверка метода из булево, имя метода.
        /// внутри метода заполняется экземпляр класса, далее данные сохраняются в таблице.
        /// </summary>
        /// <param name="startdateTimeP"></param>
        /// <param name="blP"></param>
        /// <param name="nameMetodP"></param>
        public void СreateAnEntryInTheDatabase(DateTime startdateTimeP, bool blP, string nameMetodP)
        {
            AddDBEntry bd = new AddDBEntry();
           
            bd.nameMetod = nameMetodP;
            bd.startMetod = startdateTimeP.ToString();
            bd.result = blP.ToString();
            bd.stopMetod = DateTime.Now.ToString();
            //метод для типа таймспан. время выполнения теста
            TimeSpan timeSpan = DateTime.Now - startdateTimeP;
            bd.leadTime = timeSpan.ToString();
            Context bdadd = new Context();
            bdadd.AddDBEntrys.Add(bd);
            bdadd.SaveChanges();
        }
    }

    //класс для взаимодействия с базой данных.
    // Context готовый текст.
    public class Context : DbContext  
    {
        //объект который хранится в базе данных, таблица отображается в обозревателе SQL server

        public DbSet<AddDBEntry> AddDBEntrys { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=AddDBEntrys.db"); 
        }
    }
}
