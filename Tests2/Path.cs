﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests2
{
    public class GetPath
    {
        public class Path
        {
            public string arrival_time { get; set; }
            public string departure_time { get; set; }
            public string time_stop { get; set; }
            public string name { get; set; }
        }

        public class RootObject
        {
            public List<Path> path { get; set; }
            public int requestId { get; set; }
            public int ticketId { get; set; }
        }
    }
}
