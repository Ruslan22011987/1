﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tests2.Migrations
{
    public partial class Test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Test",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    nameMetod = table.Column<string>(nullable: true),
                    result = table.Column<bool>(nullable: false),
                    startMetod = table.Column<string>(nullable: true),
                    stopMetod = table.Column<string>(nullable: true),
                    leadTime = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Test", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Test");
        }
    }
}
