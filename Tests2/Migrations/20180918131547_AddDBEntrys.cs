﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tests2.Migrations
{
    public partial class AddDBEntrys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Test",
                table: "Test");

            migrationBuilder.DropColumn(
                name: "leadTime",
                table: "Test");

            migrationBuilder.DropColumn(
                name: "result",
                table: "Test");

            migrationBuilder.DropColumn(
                name: "startMetod",
                table: "Test");

            migrationBuilder.DropColumn(
                name: "stopMetod",
                table: "Test");

            migrationBuilder.RenameTable(
                name: "Test",
                newName: "AddDBEntrys");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AddDBEntrys",
                table: "AddDBEntrys",
                column: "id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AddDBEntrys",
                table: "AddDBEntrys");

            migrationBuilder.RenameTable(
                name: "AddDBEntrys",
                newName: "Test");

            migrationBuilder.AddColumn<string>(
                name: "leadTime",
                table: "Test",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "result",
                table: "Test",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "startMetod",
                table: "Test",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "stopMetod",
                table: "Test",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Test",
                table: "Test",
                column: "id");
        }
    }
}
